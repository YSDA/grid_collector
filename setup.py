from distutils.core import setup
setup(name='grid_collector',
      version='1.0',
      description='Scripts for downloading events from'
          'GRID, designed for use with Event Index',
      author='Nikita Kazeev',
      author_email='kazeevn@yandex-team.ru',
      url='https://gitlab.cern.ch/YDF/grid_collector',
      packages=['grid_collector'],
      scripts=['grid_collector/run_process_request.py',
               'grid_collector/easy_fetch_request.py']
      )

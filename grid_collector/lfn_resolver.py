"""A class for resolving LFNs into PFNs"""

import json
import re

def normalize_lfns(lfns):
  """Returns:
  lfn without leading "LFN:"
  """
  if type(lfns) == str:
    return lfns.replace( "LFN:", "" )
  else:
    return [s.replace( "LFN:", "" ) for s in lfns]


class LFNResolver(object):
    """A class for caching lfn->pfn mapping"""

    def __init__(self):
        self.lfn_to_pfn_map = {}

    def update_from_dict(self, dict_):
        """Updates the mapping with a dict
        resolve_lfns.get_lfn2pfn_map

        Args:
            dict_: dict in form {lfn: {storage_element: [pfn1, pfn2, ...], }, ...}
        """

        self.lfn_to_pfn_map.update(dict_)

    def dump_to_json(self, file_name):
        """Writes self state to a .json file.

        Args:
            file_name: file to write
        """
        with open(file_name, 'w') as output_file:
            json.dump(self.lfn_to_pfn_map, output_file)

    def update_from_json(self, file_name):
        """Updates mapping from a .json file.

        Args:
            file_name: file to read from
        """
        with open(file_name) as input_file:
          self.lfn_to_pfn_map.update(json.load(input_file))

    def resolve_lfn(self, lfn):
        """Resolves an LFN.

        Args:
            lfn: LFN to resolve, a string

        Returns:
            dict {storage_element: [pfn1, pfn2, ...]}

        Raises:
            KeyError if lfn is not in the database
        """
        return self.lfn_to_pfn_map[lfn]

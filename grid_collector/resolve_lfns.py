#!/usr/bin/env python2
"""Resolves a list of lfns into pfns,
writes the result to disk - it can be later read
into a LFNResolver class.

Needs SetupProject LHCbDirac and a valid GRID proxy to run. Thus made into a
separate program."""

import sys
from collections import defaultdict
import argparse
import re
import logging

# DIRAC might break on our command line
true_argv = sys.argv
try:
  sys.argv = sys.argv[:1]
  from DIRAC import S_OK, S_ERROR, exit
  from DIRAC.Core.Base import Script
  Script.parseCommandLine(ignoreErrors=False)
  from DIRAC.DataManagementSystem.Client.DataManager import DataManager
finally:
  sys.argv = true_argv

from request import LFNsRequest
from lfn_resolver import LFNResolver, normalize_lfns

SE_WEIGHTS = {'CERN.*': 10, '.*-ARCHIVE':-1}
SE_DEFAULT_WEIGHT = 1

def sort_se_weighted(storage_elements, black_list = [], cut_negative = True):
  """Sorts storage_elements according to weights specified in
  SE_WEIGHTS.

  Args:
    storage_elements: a list of storage elements
    black_list: a list storage elements to drop
    cut_negative: if True will discard storage elements with negative weights

  Returns:
    a sorted list of storage elements
  """
  result = {}
  for se in storage_elements:
    if se in black_list:
      continue
    for se_re in SE_WEIGHTS.keys():
      if re.search( se_re, se ) is not None:
        result[se] = SE_WEIGHTS[se_re]
        break
    if se not in result:
      result[se] = SE_DEFAULT_WEIGHT
    if cut_negative and result[se] < 0:
      del result[se]
  sorted_se = sorted(result, key=result.get, reverse = True)
  return sorted_se


def get_lfn2pfn_map(lfns, se_black_list = []):
    """Retrieves lfn->pfn mapping.

    Args:
       lfns: a list with lfns
       se_black_list: Storage elements black list
    Returns:
       A dict with keys being lfn's and values being dicts in form
       {storage_element: [PFN1, PFN2, ]}
    """
    lfns = normalize_lfns(lfns)
    data_manager = DataManager()
    logging.info("Getting replicas list")
    res = data_manager.getReplicas(lfns, getUrl=False)
    if not res['OK']:
        raise RuntimeError("Error getting catalog replicas")
    lfn2pfn_map = defaultdict(lambda: defaultdict(list))
    se2lfns = defaultdict(set)
    for lfn, se_to_site_lfn_map in res['Value']['Successful'].iteritems():
        for se in se_to_site_lfn_map.keys():
            se2lfns[se].add(lfn)
    for se in sort_se_weighted(se2lfns.keys(), se_black_list, cut_negative = True):
        logging.info("Processing SE %s" % se)
        res_url = data_manager.getReplicaAccessUrl(
            list(se2lfns[se]), se, protocol = ['root', 'xroot'])
        logging.debug("Called data_manager.getReplicaAccessUrl(['%s',], '%s',"
                        " protocol='xroot'), got response %s" % (lfn, se, str(res_url)))
        if res_url['OK']:
            for lfn, pfn in res_url['Value']['Successful'].iteritems():
                lfn2pfn_map[lfn][se].append(pfn)
    if len(lfn2pfn_map.keys()) != len(lfns):
        missing_number = len(lfns) - len(lfn2pfn_map.keys())
        logging.error("Failed to get replicas for %d LFNs" % missing_number)
    return lfn2pfn_map


def main():
    parser = argparse.ArgumentParser(
        description="Resolves a list of lfns into pfns,"
        "writes the result to disk")
    parser.add_argument("request", type=str, help="Event Index generated request file"
                        "in json format")
    parser.add_argument("lfn_map_file", type=str, help=
                        "File to write lfn mapping")
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)
    request = LFNsRequest()
    request.load_from_json(args.request)
    lfn2pfn_map = get_lfn2pfn_map(request.get_lfn_list())
    lfn_resolver = LFNResolver()
    lfn_resolver.update_from_dict(lfn2pfn_map)
    lfn_resolver.dump_to_json(args.lfn_map_file)
    return 0

if __name__ == '__main__':
    sys.exit(main())

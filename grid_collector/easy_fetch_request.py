#!/bin/env python2
import os
import argparse
import logging
import subprocess
import sys
from itertools import imap, ifilter
from distutils.spawn import find_executable

from grid_collector.request import LFNsRequest
from grid_collector.lfn_resolver import LFNResolver, normalize_lfns
from grid_collector.request_status import RequestStatus
from grid_collector.collector import merge_root, move_and_index_jsons, cleanup
from grid_collector.resolve_lfns import get_lfn2pfn_map

DA_VINCI_SETUP_PROJECT = r"SetupProject.sh DaVinci --dev v37r2p3 gfal CASTOR lfc"
run_process_request = r"%s/.local/bin/run_process_request.py" % os.environ['HOME']
PYHTONPATH = r"%s/.local/lib/python2.7/site-packages/" % os.environ['HOME']

def get_da_vinci_environment():
    """Retrieves enviroment for DaVinci. I failed to get
    getProjectEnvironment to work on lxplus, sorry. Feel free to edit the
    command line to match your environment."""

    command = ("bash", "-c", r"source %s && env" % DA_VINCI_SETUP_PROJECT)
    proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    environment = dict(ifilter(lambda x: len(x) == 2,
                               map(lambda line: line.rstrip().split("=", 1), proc.stdout)))
    proc.communicate()
    print environment
    return environment


def main():
    parser = argparse.ArgumentParser(
        description="Fetches a request from GRID")
    parser.add_argument("request", type=str, help="Event Index generated request file"
                        " in json format")
    parser.add_argument("tmp_folder", type=str, help=
                        "folder for storing temporary files.")
    parser.add_argument("-c", "--cleanup", action="store_true",
                        help="Clean the temporary files")
    parser.add_argument("-n", "--n-jobs", type=int, default=1,
                        help="number of parallel jobs to launch")
    actions = parser.add_argument_group('actions')
    actions.add_argument("-j", "--json-folder", type=str,
                        help="If specified, move EventDisplay json files"
                        " to the folder")
    actions.add_argument("-u", "--url-prefix", type=str, default='',
                        help="Add this prefix to the json files listing. Meaningless"
                        " without -j")
    actions.add_argument("-r", "--root-file", type=str,
                        help="If specified use hadd to merge the root files")
    verbosity = parser.add_mutually_exclusive_group()
    verbosity.add_argument("-v", action="store_true", help=
                           "Set log level to WARNING")
    verbosity.add_argument("-vv", action="store_true", help=
                           "Set log level to INFO")
    verbosity.add_argument("-vvv", action="store_true", help=
                           "Set log level to DEBUG. If none of the"
                           "verbosity options are specified ERROR level is set.")
    args = parser.parse_args()
    if args.v:
        logging.basicConfig(level=logging.WARNING)
    elif args.vv:
        logging.basicConfig(level=logging.INFO)
    elif args.vvv:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.ERROR)
    if not os.path.exists(args.tmp_folder):
        os.mkdir(args.tmp_folder)
    da_vinci_environment = get_da_vinci_environment()
    request = LFNsRequest()
    request.load_from_json(args.request)
    lfn2pfn_map = get_lfn2pfn_map(request.get_lfn_list())
    lfn_resolver = LFNResolver()
    lfn_resolver.update_from_dict(lfn2pfn_map)
    lfn_map_file = os.path.join(args.tmp_folder, "lfn_map.json")
    lfn_resolver.dump_to_json(lfn_map_file)
    request_status_file = os.path.join(args.tmp_folder, "request_status.json")
    process = subprocess.Popen(r"source %s && %s" % (DA_VINCI_SETUP_PROJECT, " ".join((
        run_process_request, args.request, lfn_map_file,
        request_status_file, args.tmp_folder, "-n", str(args.n_jobs)))),
                               shell=True)
    return_code = process.wait()
    if return_code != 0:
        raise RuntimeError("run_process_request.py exited with non-zero code")
    request_status = RequestStatus()
    request_status.update_from_json(request_status_file)
    if args.json_folder:
        move_and_index_jsons(request_status, args.tmp_folder, args.json_folder, args.url_prefix)
    if args.root_file:
        merge_root(request_status, args.tmp_folder, args.root_file, da_vinci_environment)
    print(request_status.get_human_readable_status())
    if args.cleanup:
        cleanup(request_status, args.tmp_folder)
        os.remove(request_status_file)
        os.remove(lfn_map_file)


if __name__ == '__main__':
    main()

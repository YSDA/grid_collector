import os
import subprocess

from grid_collector.lfn_resolver import LFNResolver
from grid_collector.resolve_lfns import get_lfn2pfn_map
from grid_collector.request import LFNsRequest
from grid_collector.request_status import RequestStatus
from grid_collector.status_db import StatusDB
from grid_collector import process_request
from grid_collector import collector


class EventIndexRequest(object):
    """
    A class for managing Event Index requests. Basically runs the pipeline to fetch
    request from a LFNsRequest to a single .root file and folder with .json files
    for EventDisplay

    Must be run from LHCbDIRAC environment
    """

    def __init__(self, lfn_request_file, uuid, da_vinci_environment, config, n_jobs):
        """
        Args:
          lfn_request_file: a .json file name with state for LFNsRequest
          uuid: a string, used for StatusDB updating and results
            files naming.
          da_vinci_environment: a dict with environment for DaVinci, used in
            subprocess.call Best obtained from
            LHCbDIRAC.Core.Utilities.ProductionEnvironment.getProjectEnvironment
          config: a dictionary with site-specific configuration. See lbvobox27_config.json
            for example.
          n_jobs: n_jobs: maximum number parallel of jobs. A job is processing of an LFN.
        """
        self.lfn_request_file = lfn_request_file
        self.status_db = StatusDB(config['StatusDB_file_name'])
        self.uuid = uuid
        self.lfn_resolver = None
        self.request = None
        self.lfn_resolver_file = None
        self.config = config
        self.n_jobs = n_jobs
        self.da_vinci_environment = da_vinci_environment


    def process_event_index_request(self):
        """Runs the pipeline to fetch request from a LFNsRequest to a single
        .root file and folder with .json files for EventDisplay

        Must be run from LHCbDIRAC environment
        """
        self.status_db.set_status(self.uuid,
                                  self.status_db.STATUS_RUNNING,
                                  "Starting run")
        self.request = LFNsRequest()
        self.request.load_from_json(self.lfn_request_file)
        self.status_db.set_status(self.uuid,
                                  self.status_db.STATUS_RUNNING,
                                  "Resolving LFNs")
        self.lfn_resolver = LFNResolver()
        try:
            lfn2pfn_map = get_lfn2pfn_map(self.request.get_lfn_list())
        except:
            self.status_db.set_status(self.uuid,
                                      self.status_db.STATUS_FAIL,
                                      "Failed to resolve lfn->pfn map")
            raise
        self.lfn_resolver.update_from_dict(lfn2pfn_map)
        self.lfn_resolver_file = os.path.join(self.config['download_folder'],
                                              "%s_lfn2pfn_map.json" % self.uuid)
        self.lfn_resolver.dump_to_json(self.lfn_resolver_file)

        request_status_file = os.path.join(self.config['download_folder'],
                                           "%s_status.json" % self.uuid)
        job_folder = os.path.join(self.config['download_folder'], 'tmp')
        if not os.path.exists(job_folder):
            os.makedirs(job_folder)
        self.status_db.set_status(self.uuid,
                                  self.status_db.STATUS_RUNNING,
                                 "Downloading")
        try:
            process = subprocess.Popen((self.config['python_executable'],
                                        self.config['run_process_request_location'],
                                        self.lfn_request_file, self.lfn_resolver_file,
                                        request_status_file, job_folder, '-n', str(self.n_jobs)),
                                       env = self.da_vinci_environment)
            return_code = process.wait()
        except:
            self.status_db.set_status(self.uuid,
                                      self.status_db.STATUS_FAIL,
                                      "Can't run run_process_request.py")
            raise
        if return_code != 0:
            self.status_db.set_status(self.uuid,
                                      self.status_db.STATUS_FAIL,
                                      "run_process_request.py exited with non-0 code")
            raise RuntimeError("run_process_request.py exited with non-zero code")
        request_status = RequestStatus()
        request_status.update_from_json(request_status_file)
        if request_status.get_successful_lfns_count() == 0:
            self.status_db.set_status(self.uuid,
                                      self.status_db.STATUS_FAIL,
                                      "Failed to download any LFN")
            return
        self.status_db.set_status(self.uuid,
                                  self.status_db.STATUS_RUNNING,
                                  "Merging .root")
        root_path = os.path.join(self.config['download_folder'], "%s.root" % self.uuid)
        try:
            collector.merge_root(request_status, job_folder, root_path,
                                 self.da_vinci_environment)
        except:
            self.status_db.set_status(self.uuid,
                                      self.status_db.STATUS_FAIL,
                                      "hadd failed. However the root files are still there.")
            raise
        self.status_db.set_status(self.uuid,
                                  self.status_db.STATUS_RUNNING,
                                  "Collecting .json")
        json_path = os.path.join(self.config['download_folder'], self.uuid)
        collector.move_and_index_jsons(request_status, job_folder, json_path, self.uuid)
        self.status_db.set_status(self.uuid,
                                  self.status_db.STATUS_DONE,
                                  "OK")
        collector.cleanup(request_status, job_folder)

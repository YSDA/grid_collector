#!/bin/env python2
import argparse
import sys
import logging

from grid_collector.process_request import process_request
from grid_collector.request import LFNsRequest
from grid_collector.lfn_resolver import LFNResolver


def main():
  parser = argparse.ArgumentParser(
        description="Runs the event retrieval and root "
    "to json convertion for Event Index")
  parser.add_argument("request_file", type=str,
                      help="Event Index request file to run on")
  parser.add_argument("lfn_catalog", type=str,
                      help=".json for LFNResolver")
  parser.add_argument("status_file", type=str,
                      help="file to dump request status")
  parser.add_argument("request_folder", type=str,
                      help="folder for storing jobs")
  parser.add_argument("-n", "--n-jobs", type=int, default=1,
                      help="number of parallel jobs to launch")
  args = parser.parse_args()
  logging.info("Event Index collector: %s" % (args.request_file))
  request = LFNsRequest()
  request.load_from_json(args.request_file)
  lfn_resolver = LFNResolver()
  lfn_resolver.update_from_json(args.lfn_catalog)
  request_result = process_request(request, lfn_resolver, False, args.n_jobs,
                                   args.request_folder)
  request_result.dump_to_json(args.status_file)
  logging.info(request_result.get_human_readable_status())
  return 0


if __name__ == '__main__':
  sys.exit(main())

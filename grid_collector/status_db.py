from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
class EventIndexStatus(Base):
    __tablename__ = 'status'

    uuid = Column(String, primary_key=True)
    short = Column(String)
    long = Column(String)

    def __repr__(self):
        return str((self.uuid, self.short, self.long))


class StatusDB(object):
    """Class for storing statuses for Event Index request in a sqlite
    database

    """
    STATUS_INVALID = "invalid"
    STATUS_DONE = "done"
    STATUS_NEW = "new"
    STATUS_RUNNING = "running"
    STATUS_FAIL = "fail"
    STATUS_PARTIAL = "partial"
    REQUEST_NOT_FOUND = "not found"

    def __init__(self, db_file_name):
        """Args:
        db_file_name: str, file name for the sqlite database
        """
        engine = create_engine(r'sqlite:///%s' % db_file_name, echo=False)
        Base.metadata.create_all(engine)
        self.Session = sessionmaker(bind=engine)

    def get_status(self, uuid):
        """Get status for request by its uuid.

        Args:
          uuid: str, request uuid

        Returns:
          EventIndexStatus object with following parameters:
            uuid - str, request uuid
            short - str, short request status (set by set_status, usually from
             StatusDB.STATUS_*), or StatusDB.REQUEST_NOT_FOUND if
             the request with such id was not found.
            long - str, human-readable description of the status, set by set_status
        """
        session = self.Session()
        try:
            return session.query(EventIndexStatus).filter(
                EventIndexStatus.uuid == uuid).one()
        except NoResultFound:
            return EventIndexStatus(
                uuid=uuid,
                short=self.REQUEST_NOT_FOUND,
                long="Request with uuid %s not found" % uuid)

    def set_status(self, uuid, short, long):
        """Sets status of a request in the database. Creates, if request is
        not present.

        Args:
          uuid: str, request uuid, must be unique
          short: short request status, usually from StatusDB.STATUS_*
          long: str, human-readable description of the status
        """
        new_request = EventIndexStatus(
            uuid=uuid,
            short=short,
            long=long)
        session = self.Session()
        session.merge(new_request)
        session.commit()

    def pull_new_request(self):
        """Returns a EventIndexStatus for a request form th DB with short
        status self.STATUS_NEW. If no such request are in the DB,
        returns None.

        """
        session = self.Session()
        return session.query(EventIndexStatus).filter(
            EventIndexStatus.short == self.STATUS_NEW).first()


if __name__ == '__main__':
    db1 = StatusDB("test.sqlite")
    db2 = StatusDB("test.sqlite")
    print(db1.get_status("a"))
    db1.set_status("a", "32", "42")
    print(db1.get_status("a"))
    print(db2.get_status("a"))
    print(db2.get_status("b"))
    db1.set_status("b", "32", "42")
    print(db2.get_status("b"))
    db1.set_status("b", "NEWWEWE", "42")
    print(db2.get_status("b"))
    print(db1.pull_new_request())
    db1.set_status("b", "new", "42")
    print(db1.pull_new_request())

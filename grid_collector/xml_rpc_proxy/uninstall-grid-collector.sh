#!/bin/sh -ex

if [ "xroot" != "x$(whoami)" ]
then
    echo "This script must be started by root-user"
    exit 1
fi

rm /etc/cron.d/cern-eventindex-grid-collector
ps ax | egrep "python.*server\\.py" | sed 's/^ \+//' | cut -d ' ' -f1 | xargs kill -9

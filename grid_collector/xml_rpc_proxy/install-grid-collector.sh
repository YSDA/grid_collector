#!/bin/sh -ex

PID_FILE=/var/run/eventindex-grid-collector.pid
USER="dirac"
EXECUTABLE="src/server.py"
PATH_TO_EXECUTABLE="$(pwd)/$(dirname $0)/$EXECUTABLE"
PYTHON='/afs/cern.ch/sw/lcg/releases/LCG_68/Python/2.7.6/x86_64-slc6-gcc48-opt/bin/python'
LOG=/var/eventindex/log/server.log

if [ ! -x $PATH_TO_EXECUTABLE ]
then
    PATH_TO_ROOT="$(dirname $0)/$EXECUTABLE"
fi
if [ ! -x $PATH_TO_EXECUTABLE ]
then
    echo "Unable to generate .cron file, $PATH_TO_EXECUTABLE does not exist"
    exit 1
fi

if [ "xroot" != "x$(whoami)" ]
then
    echo "This script must be started by root-user"
    exit 1
fi

[ ! -d $(dirname $PID_FILE) ] && mkdir -p $(dirname $PID_FILE)

touch $PID_FILE
chown $USER $PID_FILE

echo "* * * * * $USER $PYTHON $PATH_TO_EXECUTABLE --log $LOG" > /etc/cron.d/cern-eventindex-grid-collector

# unpolite restart
PIDS=`ps ax | egrep "python.*server\\.py" | sed 's/^ \+//' | cut -d ' ' -f1`
if [[ "$PIDS" != "" ]] ; then
    kill -9 $PIDS
fi

while [ 1 = 1 ]
do
    echo "Waiting for cron to start a service"
    if ps ax | egrep -q "python.*server\\.py"
    then
        break
    fi
    sleep 5
done

date
echo "Done"

#!/usr/bin/env python
import xmlrpclib
import os
import time
import unittest
import sys

UTIL_DIR = "/opt/dirac/pro/LHCbDIRAC/TransformationSystem/Utilities/GridCollector"
sys.path.append(UTIL_DIR)

from Config import DOWNLOADS_REQUEST_DIR, DOWNLOADS_CACHE_DIR, LISTEN_PORT,\
    STATUS_RUNNING, STATUS_DONE, STATUS_FAIL, STATUS_INVALID, TYPE_ROOT

HOST = "eindex.cern.ch"
TIMEOUT = 25
TEST_EMAIL = "andrey@vdel.com"
REQUEST = [
            ["LFN:/lhcb/LHCb/Collision12/DIMUON.DST/00020350/0000/00020350_00002887_1.dimuon.dst", [1, 2, 3]],
          ]
REQUEST_LONG = [
            ["LFN:/lhcb/LHCb/Collision12/DIMUON.DST/00020350/0000/00020350_00002887_1.dimuon.dst", [1, 2, 3]],
            ["LFN:/lhcb/LHCb/Collision12/DIMUON.DST/00020350/0000/00020350_00002887_1.dimuon.dst", [1, 2, 3]],
          ]

REQUEST = [
            ["LFN:/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00020241/0001/00020241_00015578_1.bhadroncompleteevent.dst", [25619,]],
        ]
REQUEST_LONG = [
            ["LFN:/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00020241/0001/00020241_00015578_1.bhadroncompleteevent.dst", [25619,]],
            ["LFN:/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00020241/0001/00020241_00018055_1.bhadroncompleteevent.dst", [14559,]],
        ]


class TestServierGC(unittest.TestCase):

    def setUp(self):
        self.s = xmlrpclib.ServerProxy("http://%s:%d" % (HOST, LISTEN_PORT))

    def test_methods(self):
        methods = self.s.system.listMethods()
        self.assertTrue("download" in methods, msg="no download method")
        self.assertTrue("is_ready" in methods, msg="no is_ready method")

    def test_invalid_email(self):
        id = self.s.download(REQUEST,
            "valid@"
        )
        self.assertEquals(id, -1, msg="got id: %s" % id)

    def test_invalid_type(self):
        id = self.s.download(REQUEST, TEST_EMAIL, "xxx")
        self.assertEquals(id, -1, msg="got id: %s" % id)

    def test_invalid_request(self):
        id = self.s.download(REQUEST, TEST_EMAIL, TYPE_ROOT)
        self.assertEquals(id, -1, msg="got id: %s" % id)

    def test_invalid_empty_request(self):
        id = self.s.download([
            [""], []],
            TEST_EMAIL,
            TYPE_ROOT
        )
        self.assertEquals(id, -1, msg="got id: %s" % id)

    def test_invalid_None_request(self):
        id = self.s.download([[], []],
            TEST_EMAIL,
        )
        self.assertEquals(id, -1, msg="got id: %s" % id)

    def test_is_ready_invalid_id(self):
        status = self.s.is_ready("123")
        self.assertEqual(status['status'], STATUS_INVALID, "status: %s" % status)
        status = self.s.is_ready("-1")
        self.assertEqual(status['status'], STATUS_INVALID, "status: %s" % status)
        status = self.s.is_ready(-1)
        self.assertEqual(status['status'], STATUS_INVALID, "status: %s" % status)

    def test_invalid_lfn(self):
        id = self.s.download([
            ["LFN:", [1, 2, 3]], ],
            TEST_EMAIL,
            TYPE_ROOT
        )
        self.assertEquals(type(id), str, "got id: %s" % id)
        status = self.s.is_ready(id)
        self.assertEqual(status['status'], STATUS_FAIL, "status: %s" % status)

    def test_download_ok(self):
        id = self.s.download(REQUEST_LONG, TEST_EMAIL)
        self.assertEqual(type(id), str, "id is not string")
        status = False
        for _ in xrange(TIMEOUT):
            status = self.s.is_ready(id)
            print "status: %s, details: %s" % (status['status'], status['details'])
            if status['status'] == STATUS_DONE:
                break
            if status['status'] == STATUS_FAIL:
                assert False, "request failed"
            req_file = "%s/%s.%s" % (DOWNLOADS_REQUEST_DIR, id, STATUS_RUNNING)
            if not os.path.exists(req_file):
                print "WARN: no req file (%s)" % req_file
            time.sleep(1)

        assert status['status'] == STATUS_DONE, "download takes too long"
        print "details: %s" % status['details']

        assert os.path.exists("%s/%s.root" % (DOWNLOADS_CACHE_DIR, id)), "no root file"

if __name__ == '__main__':
    unittest.main()
